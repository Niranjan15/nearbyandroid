package com.niranjan.findnearby.activity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.niranjan.findnearby.R;
import com.niranjan.findnearby.adapter.ContactListViewAdapter;
import com.niranjan.findnearby.beans.UserDetailsBean;
import com.niranjan.findnearby.controller.UserDetailsController;
import com.niranjan.findnearby.service.LocationUpdateServiceNew;
import com.niranjan.findnearby.utils.AppConstants;

public class ContactDisplayActivity extends Activity
{

	private static Map<String, UserDetailsBean>	contactDetailsMap		= new TreeMap<String, UserDetailsBean>();
	private static ContactDetailsHelper			contactDetailsHelper	= new ContactDetailsHelper();
	private Set<String>							selectedPhoneNo			= new HashSet<String>();
	private ListAdapter							adapter;
	private String								filterStr;
	private SearchView							searchView;

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.main, menu);

		MenuItem searchMenuItem = menu.findItem(R.id.action_search);
		searchView = (SearchView) searchMenuItem.getActionView();

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		if (filterStr != null)
		{
			searchView.setQuery(filterStr, true);
		}
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
		{

			@Override
			public boolean onQueryTextSubmit(String query)
			{
				filterStr = query;
				setAdapterView();
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText)
			{
				filterStr = newText;
				setAdapterView();
				return false;
			}
		});
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();
		if (id == R.id.add_all_contacts)
		{
			if (selectedPhoneNo.size() < contactDetailsMap.size())
			{
				addAllContacts();
			}
			else
			{
				removeAllContacts();
			}
			return true;
		}
		if (id == R.id.setting)
		{
			Intent intent = new Intent(this, SettingActivity.class);
			startActivity(intent);
		}
		return super.onOptionsItemSelected(item);
	}

	private void submitUserDetails()
	{
		try
		{
			SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.selected_phone_number), Context.MODE_PRIVATE);
			SharedPreferences userDetails = getSharedPreferences(getString(R.string.user_details_store), Context.MODE_PRIVATE);

			UserDetailsBean userDetailsBean = new UserDetailsBean();
			userDetailsBean.setPhoneNumber(userDetails.getString(getString(R.string.phone_number), null));
			userDetailsBean.setName(userDetails.getString(getString(R.string.user_name), null));
			userDetailsBean.setFriendRange(userDetails.getFloat(getString(R.string.friend_range), AppConstants.DEFAULT_FRIEND_RANGE));
			userDetailsBean.setIsFriendMappingSync(true);

			if (new UserDetailsController().submitUserDetails(this, sharedPreferences.getAll().keySet(), userDetailsBean, null))
			{
				startService(new Intent(this, LocationUpdateServiceNew.class));
			}
		}
		catch (Exception e)
		{
			Log.e("Exception", e.getMessage());
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_display);

		contactDetailsHelper.setContactDetails(this, contactDetailsMap);
		setAdapterView();
		// Filter user if user want to remove selected friend after notification
		if (getIntent().getExtras() != null && getIntent().getExtras().getString("friendNumber") != null)
		{
			filterStr = getIntent().getExtras().getString("friendNumber");
		}
	}

	private void setAdapterView()
	{
		final SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.selected_phone_number), Context.MODE_PRIVATE);

		ListView listView = (ListView) findViewById(R.id.contacts_list);
		selectedPhoneNo = new HashSet<String>(sharedPreferences.getAll().keySet());
		adapter = new ContactListViewAdapter(this, R.layout.contacts_list_item, contactDetailsMap, selectedPhoneNo,
				getFinalContactList(contactDetailsMap, filterStr));
		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				TextView textView = (TextView) view.findViewById(R.id.contactNumberText);
				String itemValue = (String) textView.getText();

				Editor edit = sharedPreferences.edit();
				if (!selectedPhoneNo.contains(itemValue))
				{
					addContact(view, itemValue, edit);
					Toast.makeText(getApplicationContext(), "Contact Added", Toast.LENGTH_SHORT).show();
				}
				else
				{
					removeContact(view, itemValue, edit);
					Toast.makeText(getApplicationContext(), "Contact Removed", Toast.LENGTH_SHORT).show();
				}
				edit.commit();
				submitUserDetails();
			}

		});
	}

	private void addContact(View view, String itemValue, SharedPreferences.Editor edit)
	{
		if (!selectedPhoneNo.contains(itemValue))
		{
			edit.putString(itemValue, getString(R.string.selected_phone_number));
			view.findViewById(R.id.selected_friend).setVisibility(View.VISIBLE);
			selectedPhoneNo.add(itemValue);
		}
	}

	private void removeContact(View view, String itemValue, SharedPreferences.Editor edit)
	{
		if (selectedPhoneNo.contains(itemValue))
		{
			edit.remove(itemValue);
			view.findViewById(R.id.selected_friend).setVisibility(View.INVISIBLE);
			selectedPhoneNo.remove(itemValue);
		}
	}

	public void addAllContacts()
	{
		SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.selected_phone_number), Context.MODE_PRIVATE);
		ListView listView = (ListView) findViewById(R.id.contacts_list);
		View childview = null;
		TextView textView = null;
		Editor edit = sharedPreferences.edit();
		for (int position = 0; position < listView.getCount(); position++)
		{
			if (position < listView.getChildCount())
			{
				childview = listView.getChildAt(position);
				textView = (TextView) childview.findViewById(R.id.contactNumberText);
				addContact(childview, textView.getText().toString(), edit);
			}
			childview = listView.getAdapter().getView(position, null, listView);
			textView = (TextView) childview.findViewById(R.id.contactNumberText);
			addContact(childview, textView.getText().toString(), edit);
		}
		edit.commit();
		submitUserDetails();
	}

	public void removeAllContacts()
	{
		SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.selected_phone_number), Context.MODE_PRIVATE);
		ListView listView = (ListView) findViewById(R.id.contacts_list);
		View childview = null;
		TextView textView = null;
		Editor edit = sharedPreferences.edit();
		for (int position = 0; position < listView.getCount(); position++)
		{
			if (position < listView.getChildCount())
			{
				childview = listView.getChildAt(position);
				textView = (TextView) childview.findViewById(R.id.contactNumberText);
				removeContact(childview, textView.getText().toString(), edit);
			}
			childview = listView.getAdapter().getView(position, null, listView);
			textView = (TextView) childview.findViewById(R.id.contactNumberText);
			removeContact(childview, textView.getText().toString(), edit);
		}
		edit.commit();
		submitUserDetails();
	}

	public static Map<String, UserDetailsBean> getContactDetailsMap()
	{
		return contactDetailsMap;
	}

	private static ArrayList<String> getFinalContactList(final Map<String, UserDetailsBean> contactDetailsMap, String filterStr)
	{
		Set<String> contactDetails = new TreeSet<String>(new Comparator<String>()
		{
			@Override
			public int compare(String lhs, String rhs)
			{
				return lhs.toLowerCase().compareTo(rhs.toLowerCase());
			}
		});
		if (filterStr == null)
		{
			for (Entry<String, UserDetailsBean> entry : contactDetailsMap.entrySet())
			{
				contactDetails.add(entry.getValue().getName() + AppConstants.CONTACT_NAME_DELIMITER + entry.getKey());
			}
		}
		else
		{
			for (Entry<String, UserDetailsBean> entry : contactDetailsMap.entrySet())
			{
				if (entry.getKey().toLowerCase().contains(filterStr.toLowerCase()) || entry.getValue().getName().toLowerCase().contains(filterStr.toLowerCase()))
				{
					contactDetails.add(entry.getValue().getName() + AppConstants.CONTACT_NAME_DELIMITER + entry.getKey());
				}
			}
		}
		return new ArrayList<String>(contactDetails);
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		if (Intent.ACTION_SEARCH.equals(intent.getAction()))
		{
			filterStr = intent.getStringExtra(SearchManager.QUERY);
			setAdapterView();
		}
	}

	@Override
	public void onBackPressed()
	{
		if (searchView != null && getIntent().getExtras() != null && getIntent().getExtras().getString("friendNumber") != null)
		{
			filterStr = null;
			startActivity(new Intent(this, MainActivity.class));
			getIntent().removeExtra("friendNumber");
			this.finish();
		}
		else
		{
			super.onBackPressed();
		}
	}
}
