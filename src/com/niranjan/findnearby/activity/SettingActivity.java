package com.niranjan.findnearby.activity;

import java.util.Calendar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.niranjan.findnearby.R;
import com.niranjan.findnearby.beans.UserDetailsBean;
import com.niranjan.findnearby.controller.UserDetailsController;
import com.niranjan.findnearby.utils.CommonUtils;
import com.niranjan.findnearby.utils.DatePickerFragment;

public class SettingActivity extends Activity
{

	private DatePickerFragment	datePickerFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);

		SharedPreferences userDetails = getSharedPreferences(getString(R.string.user_details_store), Context.MODE_PRIVATE);
		// Phone number
		EditText editText = (EditText) findViewById(R.id.phone_number_input);
		editText.setText(userDetails.getString(getString(R.string.phone_number), ""));
		// User Name
		editText = (EditText) findViewById(R.id.user_name_input);
		editText.setText(userDetails.getString(getString(R.string.user_name), ""));
		// Range input
		editText = (EditText) findViewById(R.id.friend_range_input);
		editText.setText(String.valueOf(userDetails.getFloat(getString(R.string.friend_range), 10f)));
		// DOB input
		editText = (EditText) findViewById(R.id.user_dob_input);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(userDetails.getLong(getString(R.string.date_of_birth), 0));
		datePickerFragment = new DatePickerFragment(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
		editText.setText(datePickerFragment.getDateFormat().format(calendar.getTime()));
	}

	/**
	 * @param view
	 */
	public void saveUserDetails(View view)
	{
		SharedPreferences userDetails = getSharedPreferences(getString(R.string.user_details_store), Context.MODE_PRIVATE);

		EditText editText = (EditText) findViewById(R.id.phone_number_input);
		UserDetailsBean detailsBean = new UserDetailsBean();
		if (editText.getText() == null || editText.getText().toString().isEmpty())
		{
			Toast.makeText(this, "Please enter the phone number", Toast.LENGTH_SHORT).show();
			return;
		}
		detailsBean.setPhoneNumber(editText.getText().toString());

		editText = (EditText) findViewById(R.id.user_name_input);
		if (editText.getText() == null || editText.getText().toString().isEmpty())
		{
			Toast.makeText(this, "Please enter the user name", Toast.LENGTH_SHORT).show();
			return;
		}
		detailsBean.setName(editText.getText().toString());

		editText = (EditText) findViewById(R.id.user_dob_input);
		if (editText.getText() == null || editText.getText().toString().isEmpty())
		{
			Toast.makeText(this, "Please enter the date of birth", Toast.LENGTH_SHORT).show();
			return;
		}
		Calendar calendar = CommonUtils.getDobCalendar(datePickerFragment.getDay(), datePickerFragment.getMonth(), datePickerFragment.getYear());
		detailsBean.setDob(calendar.getTime());

		editText = (EditText) findViewById(R.id.friend_range_input);
		if (editText.getText() == null || editText.getText().toString().isEmpty())
		{
			Toast.makeText(this, "Please enter the distance", Toast.LENGTH_SHORT).show();
			return;
		}
		detailsBean.setFriendRange(Float.parseFloat(editText.getText().toString()));

		Editor edit = userDetails.edit();
		edit.putString(getString(R.string.phone_number), detailsBean.getPhoneNumber());
		edit.putString(getString(R.string.user_name), detailsBean.getName());
		edit.putFloat(getString(R.string.friend_range), detailsBean.getFriendRange());
		edit.putLong(getString(R.string.date_of_birth), detailsBean.getDob().getTime());
		edit.commit();
		detailsBean.setIsFriendMappingSync(false);

		boolean submitStatus = new UserDetailsController().submitUserDetails(this, null, detailsBean, null);
		if (submitStatus)
		{
			Intent intent = new Intent(this, ContactDisplayActivity.class);
			startActivity(intent);
		}
	}

	/**
	 * @param view
	 */
	public void showDatePicker(View view)
	{
		// showDatePicker(view);
		datePickerFragment.setDobView(view);
		datePickerFragment.show(getFragmentManager(), "datePicker");
	}
}
