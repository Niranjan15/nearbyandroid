package com.niranjan.findnearby.activity;

import java.util.Calendar;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.niranjan.findnearby.R;
import com.niranjan.findnearby.beans.UserDetailsBean;
import com.niranjan.findnearby.controller.UserDetailsController;
import com.niranjan.findnearby.utils.CommonUtils;
import com.niranjan.findnearby.utils.DatePickerFragment;

public class MainActivity extends Activity
{
	private DatePickerFragment	datePickerFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		SharedPreferences userDetails = getSharedPreferences(getString(R.string.user_details_store), Context.MODE_PRIVATE);
		if (userDetails.getString(getString(R.string.phone_number), null) != null)
		{
			Bundle extras = getIntent().getExtras();
			setContentView(R.layout.activity_contact_display);
			Intent intent = new Intent(this, ContactDisplayActivity.class);
			if (extras != null && extras.getInt("mId") > 0)
			{
				((NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(extras.getInt("mId"));
				intent.putExtra("friendNumber", extras.get("friendNumber").toString());
			}
			startActivity(intent);
			this.finish();
		}
		else
		{
			Calendar cal = Calendar.getInstance();
			datePickerFragment = new DatePickerFragment(cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));
			setContentView(R.layout.activity_user_registration);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		return true;
	}

	public void saveUserDetails(View view)
	{
		SharedPreferences userDetails = getSharedPreferences(getString(R.string.user_details_store), Context.MODE_PRIVATE);

		EditText editText = (EditText) findViewById(R.id.phone_number_input);
		UserDetailsBean detailsBean = new UserDetailsBean();
		if (editText.getText() == null || editText.getText().toString().isEmpty())
		{
			Toast.makeText(this, "Please enter the phone number", Toast.LENGTH_SHORT).show();
			return;
		}
		detailsBean.setPhoneNumber(editText.getText().toString());

		editText = (EditText) findViewById(R.id.user_name_input);
		if (editText.getText() == null || editText.getText().toString().isEmpty())
		{
			Toast.makeText(this, "Please enter the user name", Toast.LENGTH_SHORT).show();
			return;
		}
		detailsBean.setName(editText.getText().toString());
		editText = (EditText) findViewById(R.id.user_dob);
		if (editText.getText() == null || editText.getText().toString().isEmpty())
		{
			Toast.makeText(this, "Please enter the date of birth", Toast.LENGTH_SHORT).show();
			return;
		}
		Calendar calendar = CommonUtils.getDobCalendar(datePickerFragment.getDay(), datePickerFragment.getMonth(), datePickerFragment.getYear());
		detailsBean.setDob(calendar.getTime());
		detailsBean.setFriendRange(5);

		Editor edit = userDetails.edit();
		edit.putString(getString(R.string.phone_number), detailsBean.getPhoneNumber());
		edit.putString(getString(R.string.user_name), detailsBean.getName());
		edit.putFloat(getString(R.string.friend_range), detailsBean.getFriendRange());
		edit.putLong(getString(R.string.date_of_birth), detailsBean.getDob().getTime());
		edit.commit();
		detailsBean.setIsFriendMappingSync(false);

		boolean submitStatus = new UserDetailsController().submitUserDetails(this, null, detailsBean, null);
		if (submitStatus)
		{
			Intent intent = new Intent(this, ContactDisplayActivity.class);
			startActivity(intent);
		}
	}
	
	public void showDatePicker(View view)
	{
		// showDatePicker(view);
		datePickerFragment.setDobView(view);
		datePickerFragment.show(getFragmentManager(), "datePicker");
	}
}
