package com.niranjan.findnearby.activity;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.PhoneLookup;
import android.util.Log;

import com.niranjan.findnearby.R;
import com.niranjan.findnearby.beans.UserDetailsBean;

public class ContactDetailsHelper
{
	private static Bitmap	DEFAULT_IMG_BITMAP	= null;

	public void setContactDetails(Context context, Map<String, UserDetailsBean> contactDetailsMap)
	{
		if (contactDetailsMap.size() != 0)
		{
			return;
		}
		Cursor cursor = null;
		try
		{
			ContentResolver cr = context.getContentResolver(); // Activity/Application android.content.Context
			cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
			if (cursor.moveToFirst())
			{
				UserDetailsBean detailsBean = null;
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = calculateInSampleSize(options, 65, 65);
				InputStream cPhotoStream = null;
				do
				{
					String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

					if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
					{
						Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]
						{
								ContactsContract.CommonDataKinds.Phone.NUMBER,
								ContactsContract.Contacts.DISPLAY_NAME,
								ContactsContract.Contacts.PHOTO_URI
						}, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]
						{
							id
						}, null);
						while (pCur.moveToNext())
						{
							detailsBean = new UserDetailsBean();
							detailsBean.setPhoneNumber(pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
							detailsBean.setName(pCur.getString(pCur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
							detailsBean.setPhotoUri(pCur.getString(pCur.getColumnIndex(ContactsContract.Contacts.PHOTO_URI)));
							if (detailsBean.getPhotoUri() == null)
							{
								setDefaultImg(context, detailsBean, options);
							}
							else
							{
								cPhotoStream = null;
								try
								{
									cPhotoStream = context.getContentResolver().openAssetFileDescriptor(Uri.parse(detailsBean.getPhotoUri()), "r")
											.createInputStream();
									detailsBean.setContactPhoto(BitmapFactory.decodeStream(cPhotoStream, null, options));
								}
								catch (IOException e)
								{
									Log.e("Exception", e.getMessage());
									setDefaultImg(context, detailsBean, options);
								}
								finally
								{
									if (cPhotoStream != null)
									{
										try
										{
											cPhotoStream.close();
										}
										catch (IOException e)
										{
											Log.e("Exception", e.getMessage());
										}
									}
								}
							}
							contactDetailsMap.put(detailsBean.getPhoneNumber(), detailsBean);
							break;
						}
						pCur.close();
					}

				}
				while (cursor.moveToNext());
			}
		}
		catch (NumberFormatException e)
		{
			Log.e("Exception", e.getMessage());
		}
		finally
		{
			if (cursor != null)
			{
				cursor.close();
			}
		}
	}

	private void setDefaultImg(Context context, UserDetailsBean detailsBean, Options options)
	{
		if (DEFAULT_IMG_BITMAP == null)
		{
			DEFAULT_IMG_BITMAP = BitmapFactory.decodeResource(context.getResources(), R.drawable.contact, options);
		}
		detailsBean.setContactPhoto(DEFAULT_IMG_BITMAP);
	}

	private void getContactDetailsByPhoneLookup(Context context, Map<String, UserDetailsBean> contactDetailsMap)
	{
		Cursor cursor = null;
		try
		{
			cursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, new String[]
			{
					ContactsContract.Contacts._ID,
					ContactsContract.CommonDataKinds.Phone.NUMBER,
					ContactsContract.Contacts.DISPLAY_NAME,
					ContactsContract.Contacts.PHOTO_URI
			}, null, null, null);
			if (cursor == null)
			{
				return;
			}
			UserDetailsBean detailsBean = null;
			while (cursor.moveToNext())
			{
				detailsBean = new UserDetailsBean();
				detailsBean.setContactId(cursor.getLong(cursor.getColumnIndex(ContactsContract.Contacts._ID)));
				detailsBean.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PhoneLookup.NUMBER)));
				detailsBean.setName(cursor.getString(cursor.getColumnIndex(PhoneLookup.DISPLAY_NAME)));
				detailsBean.setPhotoUri(cursor.getString(cursor.getColumnIndex(PhoneLookup.PHOTO_URI)));
				contactDetailsMap.put(detailsBean.getPhoneNumber(), detailsBean);
			}
		}
		catch (Exception e)
		{
			Log.e("Exception", e.getMessage());
		}
		finally
		{
			if (cursor != null)
			{
				cursor.close();
			}
		}
	}

	private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
	{
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth)
		{

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth)
			{
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}
}
