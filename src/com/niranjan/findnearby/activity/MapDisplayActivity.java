package com.niranjan.findnearby.activity;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class MapDisplayActivity extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		showMapActivity(intent);
	}

	private void showMapActivity(Intent intent)
	{

		StringBuilder uriBuilder = new StringBuilder("google.navigation:q=");
		uriBuilder.append(intent.getExtras().getDouble("latitude")).append(",").append(intent.getExtras().getDouble("longitude"));

		Intent mapIntent = new Intent(Intent.ACTION_VIEW);
		mapIntent.setData(Uri.parse(uriBuilder.toString()));
		startActivity(mapIntent);

		this.finish();
		((NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(intent.getExtras().getInt("mId"));
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		showMapActivity(intent);
	}
}
