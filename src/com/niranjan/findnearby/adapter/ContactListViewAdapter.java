package com.niranjan.findnearby.adapter;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.niranjan.findnearby.R;
import com.niranjan.findnearby.beans.UserDetailsBean;
import com.niranjan.findnearby.utils.AppConstants;

public class ContactListViewAdapter extends ArrayAdapter<String>
{

	private final Context						context;
	private final Map<String, UserDetailsBean>	items;
	private final int							listItemId;
	private final List<String>					contactNumberSet;
	private Set<String>							selectedItems;
	private Random								rnd	= new Random();

	public ContactListViewAdapter(Context context, final int listItemId, final Map<String, UserDetailsBean> contactDetailsMap, Set<String> selectedItems,
			List<String> contactNumberSet)
	{
		super(context, listItemId, contactNumberSet);
		this.contactNumberSet = contactNumberSet;
		this.context = context;
		this.items = contactDetailsMap;
		this.listItemId = listItemId;
		this.selectedItems = selectedItems;
	}

	@Override
	@SuppressLint("ViewHolder")
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(listItemId, parent, false);

		TextView contactNumberTextView = (TextView) rowView.findViewById(R.id.contactNumberText);
		contactNumberTextView.setText(contactNumberSet.get(position).split(AppConstants.CONTACT_NAME_DELIMITER)[1]);

		int argb = Color.argb(200, rnd.nextInt(200), rnd.nextInt(150), rnd.nextInt(150));
		TextView contactNameTextView = (TextView) rowView.findViewById(R.id.contactNameText);
		contactNameTextView.setText(items.get(contactNumberTextView.getText()).getName());
		ImageView contactImgView;
		contactImgView = (ImageView) rowView.findViewById(R.id.contact_img);
		contactImgView.setImageBitmap(items.get(contactNumberTextView.getText()).getContactPhoto());
		contactImgView.setBackgroundColor(argb);
		// rowView.findViewById(R.id.contact_grid).setBackgroundColor(argb);

		if (selectedItems.contains(contactNumberTextView.getText()))
		{
			rowView.findViewById(R.id.selected_friend).setVisibility(View.VISIBLE);
		}
		else
		{
			rowView.findViewById(R.id.selected_friend).setVisibility(View.INVISIBLE);
		}
		return rowView;
	}
}
