package com.niranjan.findnearby.service;

import java.util.List;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.niranjan.findnearby.R;
import com.niranjan.findnearby.beans.UserDetailsBean;
import com.niranjan.findnearby.controller.UserDetailsController;
import com.niranjan.findnearby.utils.AppConstants;

public class LocationUpdateServiceNew extends Service
{

	private static final String	TAG					= "LocationUpdateService";
	private LocationManager		mLocationManager	= null;
	private static final int	LOCATION_INTERVAL	= 60 * 1000;
	private static final float	LOCATION_DISTANCE	= 0f;
	private static Context		context;

	class LocationListener implements android.location.LocationListener
	{
		Location	mLastLocation;

		public LocationListener(String provider)
		{
			// Log.i(TAG, "LocationListener " + provider);
			mLastLocation = new Location(provider);
		}

		@Override
		public void onLocationChanged(Location location)
		{
			// Log.i(TAG, "onLocationChanged: " + location);
			SharedPreferences userDetails = getSharedPreferences(getString(R.string.user_details_store), Context.MODE_PRIVATE);
			UserDetailsBean detailsBean = new UserDetailsBean();
			detailsBean.setPhoneNumber(userDetails.getString(getString(R.string.phone_number), null));
			detailsBean.setName(userDetails.getString(getString(R.string.user_name), null));
			detailsBean.setFriendRange(userDetails.getFloat(getString(R.string.friend_range), AppConstants.DEFAULT_FRIEND_RANGE));
			detailsBean.setIsFriendMappingSync(false);
			new UserDetailsController().submitUserDetails(context, null, detailsBean, location);

			mLastLocation.set(location);
		}

		@Override
		public void onProviderDisabled(String provider)
		{
			// Log.i(TAG, "onProviderDisabled: " + provider);
		}

		@Override
		public void onProviderEnabled(String provider)
		{
			// Log.i(TAG, "onProviderEnabled: " + provider);
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras)
		{
			// Log.i(TAG, "onStatusChanged: " + provider);
		}
	}

	private String				bestProvider;
	private LocationListener	listener;

	@Override
	public IBinder onBind(Intent arg0)
	{
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		Log.i(TAG, "onStartCommand");
		super.onStartCommand(intent, flags, startId);
		return START_STICKY;
	}

	@Override
	public void onCreate()
	{
		Log.i(TAG, "onCreate");
		initializeLocationManager();
		try
		{
			mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, listener);
		}
		catch (java.lang.SecurityException ex)
		{
			Log.i(TAG, "fail to request location update, ignore", ex);
		}
		catch (IllegalArgumentException ex)
		{
			Log.d(TAG, "network provider does not exist, " + ex.getMessage());
		}
		// try
		// {
		// mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, mLocationListeners[0]);
		// }
		// catch (java.lang.SecurityException ex)
		// {
		// Log.i(TAG, "fail to request location update, ignore", ex);
		// }
		// catch (IllegalArgumentException ex)
		// {
		// Log.d(TAG, "gps provider does not exist " + ex.getMessage());
		// }
	}

	@Override
	public void onDestroy()
	{
		Log.i(TAG, "onDestroy");
		super.onDestroy();
		if (mLocationManager != null)
		{
			// for (int i = 0; i < mLocationListeners.length; i++)
			// {
			try
			{
				mLocationManager.removeUpdates(listener);
			}
			catch (Exception ex)
			{
				Log.i(TAG, "fail to remove location listners, ignore", ex);
			}
			// }
		}
	}

	private void initializeLocationManager()
	{
		Log.i(TAG, "initializeLocationManager");
		context = this;
		if (mLocationManager == null)
		{
			mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_FINE);
			criteria.setAltitudeRequired(false);
			criteria.setBearingRequired(false);
			criteria.setCostAllowed(true);
			criteria.setPowerRequirement(Criteria.POWER_LOW);
			bestProvider = mLocationManager.getBestProvider(criteria, true);

			if (bestProvider == null)
			{
				List<String> providers = mLocationManager.getProviders(criteria, true);
				for (String provider : providers)
				{
					if (mLocationManager.getLastKnownLocation(provider) != null)
					{
						bestProvider = provider;
						break;
					}
				}
			}

			mLocationManager.getLastKnownLocation(bestProvider);
			listener = new LocationListener(bestProvider);
		}
	}
}
