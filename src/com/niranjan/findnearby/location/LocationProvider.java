package com.niranjan.findnearby.location;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

public class LocationProvider
{
	private static final String		TAG			= "DEBUG";
	private static final String[]	provider	= new String[]
												{
			LocationManager.GPS_PROVIDER,
			LocationManager.NETWORK_PROVIDER,
			LocationManager.PASSIVE_PROVIDER
												};

	public Location getLocationByProvider(Context context)
	{
		Location location = null;
		// LocationManager locationManager = (LocationManager) context.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		try
		{
			for (int i = 0; i < provider.length; i++)
			{
				if (locationManager.isProviderEnabled(provider[i]))
				{
					// locationManager.requestLocationUpdates(provider[i], 0, 0, locationListenerGps);
					location = locationManager.getLastKnownLocation(provider[i]);
					if (location != null)
					{
						break;
					}
				}
			}
		}
		catch (IllegalArgumentException e)
		{
			Log.d(TAG, "Cannot acces Provider " + provider);
		}
		return location;
	}

}