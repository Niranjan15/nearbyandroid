package com.niranjan.findnearby.beans;

import java.io.Serializable;
import java.util.Date;

import android.graphics.Bitmap;

public class UserDetailsBean implements Serializable
{
	private static final long	serialVersionUID	= 1L;

	private long				contactId;
	private String				phoneNumber;
	private String				name;
	private String				photoUri;
	private Boolean				isFriendMappingSync;
	private Bitmap				contactPhoto;
	private float				friendRange;
	private Date				dob;

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = getOnlyPhoneNumber(phoneNumber);
	}

	private String getOnlyPhoneNumber(String phoneNumber2)
	{
		if (phoneNumber2 != null && !phoneNumber2.isEmpty())
		{
			phoneNumber2 = phoneNumber2.replace(" ", "");
			phoneNumber2 = phoneNumber2.replace("-", "");
			if (phoneNumber2.length() > 10)
			{
				return phoneNumber2.substring((phoneNumber2.length() - 10), (phoneNumber2.length()));
			}
		}
		return phoneNumber2;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getPhotoUri()
	{
		return photoUri;
	}

	public void setPhotoUri(String photoUri)
	{
		this.photoUri = photoUri;
	}

	public Boolean getIsFriendMappingSync()
	{
		return isFriendMappingSync;
	}

	public void setIsFriendMappingSync(Boolean isFriendMappingSync)
	{
		this.isFriendMappingSync = isFriendMappingSync;
	}

	public long getContactId()
	{
		return contactId;
	}

	public void setContactId(long contactId)
	{
		this.contactId = contactId;
	}

	public Bitmap getContactPhoto()
	{
		return contactPhoto;
	}

	public void setContactPhoto(Bitmap contactPhoto)
	{
		this.contactPhoto = contactPhoto;
	}

	public float getFriendRange()
	{
		return friendRange;
	}

	public void setFriendRange(float friendRange)
	{
		this.friendRange = friendRange;
	}

	public Date getDob()
	{
		return dob;
	}

	public void setDob(Date dob)
	{
		this.dob = dob;
	}

}
