package com.niranjan.findnearby.utils;

public interface AppConstants
{
	String	CONTACT_NAME_DELIMITER	= "##;##";
	float	DEFAULT_FRIEND_RANGE	= 10;
}
