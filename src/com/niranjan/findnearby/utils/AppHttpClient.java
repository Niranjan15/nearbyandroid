package com.niranjan.findnearby.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import android.util.Log;

public class AppHttpClient
{

	/**
	 * @param actionUrl
	 * @return
	 */
	public String getRequest(String actionUrl)
	{
		HttpURLConnection urlConnection = null;
		try
		{
			URL url = new URL(actionUrl);
			urlConnection = (HttpURLConnection) url.openConnection();
			return readInputStream(urlConnection);
		}
		catch (Exception e)
		{
			Log.e("Excepion", e.getMessage());
		}
		finally
		{
			if (urlConnection != null)
			{
				urlConnection.disconnect();
			}
		}
		return null;
	}

	/**
	 * @param urlConnection
	 * @return
	 */
	private String readInputStream(HttpURLConnection urlConnection)
	{
		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream())))
		{
			StringBuilder stringBuilder = new StringBuilder();
			String read = null;
			while ((read = bufferedReader.readLine()) != null)
			{
				stringBuilder.append(read);
			}
			return stringBuilder.toString();
		}
		catch (Exception e)
		{
			Log.e("Exception", e.getMessage());
		}
		return null;
	}

	/**
	 * @param actionUrl
	 * @param data
	 * @return
	 * @throws IOException
	 */
	public String submitRequest(String actionUrl, String data) throws IOException
	{
		URL url;
		HttpURLConnection urlConnection = null;
		OutputStream outputStream = null;
		try
		{
			url = new URL(actionUrl);
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setReadTimeout(10000);
			urlConnection.setConnectTimeout(15000);
			// urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
			urlConnection.setRequestMethod("POST");
			urlConnection.setFixedLengthStreamingMode(data.getBytes().length);
			urlConnection.connect();
			int responseCode = urlConnection.getResponseCode();

			if (responseCode == 200)
			{
				outputStream = urlConnection.getOutputStream();
				try (BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream)))
				{
					bufferedWriter.write(data);
					return readInputStream(urlConnection);
				}
				catch (Exception e)
				{
					Log.e("Exception", e.getMessage());
				}
			}
		}
		catch (Exception e)
		{
			Log.e("Exception", e.getMessage());
		}
		finally
		{
			if (urlConnection != null)
			{
				urlConnection.disconnect();
			}

			if (outputStream != null)
			{
				outputStream.close();
			}
		}
		return null;
	}
}
