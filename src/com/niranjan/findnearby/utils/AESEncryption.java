package com.niranjan.findnearby.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;
import android.util.Log;

public class AESEncryption
{
	private static final String	AES						= "AES";
	private static final String	UTF_8					= "UTF-8";
	private static final String	AES_CBC_PKCS5PADDING	= "AES/CBC/PKCS5PADDING";
	private static final String	key						= "AllIsWellniranja";
	private static final String	initVector				= "RandomInitVector";

	public static String encrypt(String value)
	{
		try
		{
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(UTF_8));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(UTF_8), AES);

			Cipher cipher = Cipher.getInstance(AES_CBC_PKCS5PADDING);
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

			byte[] encrypted = cipher.doFinal(value.getBytes());
			return Base64.encodeToString(encrypted, Base64.DEFAULT);
		}
		catch (Exception ex)
		{
			Log.e("Exception", ex.getMessage());
		}
		return null;
	}

	public static String decrypt(String encrypted)
	{
		try
		{
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(UTF_8));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(UTF_8), AES);

			Cipher cipher = Cipher.getInstance(AES_CBC_PKCS5PADDING);
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

			byte[] original = cipher.doFinal(Base64.decode(encrypted, Base64.DEFAULT));

			return new String(original);
		}
		catch (Exception ex)
		{
			Log.e("Exception", ex.getMessage());
		}
		return null;
	}
}
