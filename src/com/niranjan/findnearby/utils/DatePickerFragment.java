package com.niranjan.findnearby.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener
{

	private static final String	pattern		= "dd-MM-yyyy";
	private SimpleDateFormat	dateFormat	= new SimpleDateFormat(pattern);
	private int					year;
	private int					month;
	private int					day;
	private View				dobView;

	public DatePickerFragment(int day, int month, int year)
	{
		this.day = day;
		this.month = month;
		this.year = year;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		return new DatePickerDialog(getActivity(), this, year, month, day);
	}

	public void onDateSet(DatePicker view, int year, int month, int day)
	{
		this.year = year;
		this.month = month;
		this.day = day;
		Calendar dobCalendar = CommonUtils.getDobCalendar(day, month, year);
		((EditText) this.dobView).setText(dateFormat.format(dobCalendar.getTime()));
		// view.init(year, month, day, (OnDateChangedListener) this);
	}

	public int getMonth()
	{
		return month;
	}

	public void setMonth(int month)
	{
		this.month = month;
	}

	public int getYear()
	{
		return year;
	}

	public int getDay()
	{
		return day;
	}

	public SimpleDateFormat getDateFormat()
	{
		return dateFormat;
	}

	public View getDobView()
	{
		return dobView;
	}

	public void setDobView(View dobView)
	{
		this.dobView = dobView;
	}

}
