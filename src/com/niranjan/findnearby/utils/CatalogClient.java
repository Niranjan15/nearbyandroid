package com.niranjan.findnearby.utils;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;
import android.util.Log;

public class CatalogClient extends AsyncTask<String, String, String>
{

	@Override
	protected String doInBackground(String... params)
	{
		URL url;
		HttpURLConnection urlConnection = null;

		try
		{
			url = new URL(params[0]);
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setReadTimeout(10000);
			urlConnection.setConnectTimeout(15000);
			urlConnection.setDoInput(true);
			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
			urlConnection.setRequestMethod("POST");
			urlConnection.connect();
			OutputStream outputStream = null;
			try
			{
				outputStream = new BufferedOutputStream(urlConnection.getOutputStream());
				outputStream.write(params[1].getBytes());
				outputStream.flush();
				int responseCode = urlConnection.getResponseCode();
				Log.i("ResponceCode:", String.valueOf(responseCode));
				if (responseCode == 200)
				{
					return readInputStream(urlConnection);
				}
			}
			catch (Exception e)
			{
				Log.e("Exception", e.getMessage());
			}
			finally
			{
				if (outputStream != null)
				{
					outputStream.close();
				}
			}

		}
		catch (Exception e)
		{
			Log.e("Exception", e.getMessage());
		}
		finally
		{
			if (urlConnection != null)
			{
				urlConnection.disconnect();
			}
		}

		return null;
	}

	/**
	 * @param urlConnection
	 * @return
	 */
	private String readInputStream(HttpURLConnection urlConnection)
	{
		BufferedReader bufferedReader = null;
		try
		{
			bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			StringBuilder stringBuilder = new StringBuilder();
			String read = null;
			while ((read = bufferedReader.readLine()) != null)
			{
				stringBuilder.append(read);
			}
			return stringBuilder.toString();
		}
		catch (Exception e)
		{
			Log.e("Exception", e.getMessage());
		}
		finally
		{
			if (bufferedReader != null)
			{
				try
				{
					bufferedReader.close();
				}
				catch (IOException e)
				{
					Log.e("Exception", e.getMessage());
				}
			}
		}
		return null;
	}
}