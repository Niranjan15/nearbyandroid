package com.niranjan.findnearby.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.PhoneLookup;
import android.util.Log;

public class ContactSearchUtil
{
	public static String getUserProfile(Context context, String phoneNumber)
	{
		Cursor cursor = null;
		try
		{
			Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
			cursor = context.getContentResolver().query(uri, new String[]
			{
					PhoneLookup.NUMBER,
					PhoneLookup.DISPLAY_NAME
			}, null, null, null);
			if (cursor == null)
			{
				return null;
			}
			try
			{
				while (cursor.moveToNext())
				{
					return cursor.getString(cursor.getColumnIndex(Contacts.DISPLAY_NAME));
				}
			}
			finally
			{
				cursor.close();
			}
		}
		catch (Exception e)
		{
			Log.e("Exception", e.getMessage());
		}
		finally
		{
			if (cursor != null)
			{
				cursor.close();
			}
		}

		return null;
	}
}
