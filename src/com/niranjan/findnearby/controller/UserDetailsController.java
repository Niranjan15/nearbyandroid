package com.niranjan.findnearby.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.niranjan.findnearby.R;
import com.niranjan.findnearby.beans.UserDetailsBean;
import com.niranjan.findnearby.location.LocationProvider;
import com.niranjan.findnearby.notification.AppNotificationBuilder;
import com.niranjan.findnearby.utils.AESEncryption;
import com.niranjan.findnearby.utils.CatalogClient;
import com.niranjan.findnearby.utils.URLConstants;

public class UserDetailsController
{
	public boolean submitUserDetails(Context context, Set<String> selectedPHNumbers, UserDetailsBean detailsBean, Location location)
	{
		try
		{
			if (location == null)
			{
				location = new LocationProvider().getLocationByProvider(context);
				if (selectedPHNumbers != null && location == null)
				{
					try
					{
						Toast.makeText(context, "Please on your GPS", Toast.LENGTH_SHORT).show();
					}
					catch (Exception e)
					{
						Log.e("Exception", e.getMessage());
					}
					return false;
				}
			}
			String threadName = Thread.currentThread().getName();

			ServerInterfaceThread serverInterfaceThread = new ServerInterfaceThread();
			if (context != null)
			{
				serverInterfaceThread.setContext(context);
			}
			serverInterfaceThread.setDetailsBean(detailsBean);
			serverInterfaceThread.setLocation(location);
			serverInterfaceThread.setSelectedPHNumbers(selectedPHNumbers);
			Thread thread = new Thread(serverInterfaceThread);
			thread.setName("Server Interface Thread");
			thread.start();
			thread.setName(threadName);
		}
		catch (Exception e)
		{
			Log.e("Exception", e.getMessage());
		}
		return true;
	}

	class ServerInterfaceThread implements Runnable
	{

		private static final String	RESPONCE	= "responce";
		private Context				context;
		private Set<String>			selectedPHNumbers;
		private UserDetailsBean		detailsBean;
		private Location			location;

		@Override
		public void run()
		{
			try
			{
				JSONObject userDetails = new JSONObject();
				userDetails.put("userName", detailsBean.getName());
				userDetails.put("userPhoneNumber", AESEncryption.encrypt(detailsBean.getPhoneNumber()));
				userDetails.put("friendRange", detailsBean.getFriendRange());
				if (detailsBean.getDob() != null)
				{
					userDetails.put("dob", detailsBean.getDob().getTime());
				}
				userDetails.put("userFriendsPhNO", new JSONArray(encryptAllPhoneNumbers()));
				userDetails.put("latitude", location.getLatitude());
				userDetails.put("longitude", location.getLongitude());
				userDetails.put("isFriendMappingSync", detailsBean.getIsFriendMappingSync());
				CatalogClient catalogClient = new CatalogClient();
				catalogClient.execute(new String[]
				{
						URLConstants.URL + URLConstants.INSERT_USER,
						userDetails.toString()
				});
				String responceObject = catalogClient.get();
				if (responceObject != null)
				{
					JSONObject respJson = new JSONObject(responceObject);
					// Log.d("Responce", respJson.toString());
					if (!respJson.isNull(RESPONCE))
					{
						JSONArray jsonArray = respJson.getJSONArray(RESPONCE);
						for (int i = 0; i < jsonArray.length(); i++)
						{
							new AppNotificationBuilder().notify(context, context.getString(R.string.app_name), jsonArray.getJSONObject(i));
						}
					}
				}
			}
			catch (Exception e)
			{
				Log.e("Exception", e.getMessage());
			}
		}

		/**
		 * @param selectedPHNumbers2
		 * @return
		 */
		private Set<String> encryptAllPhoneNumbers()
		{

			if (selectedPHNumbers != null)
			{
				Set<String> encryptedPHNumbers = new HashSet<String>();
				Iterator<String> iterator = selectedPHNumbers.iterator();
				while (iterator.hasNext())
				{
					encryptedPHNumbers.add(AESEncryption.encrypt(iterator.next()));
					iterator.remove();
				}
				return encryptedPHNumbers;
			}
			return null;
		}

		public Context getContext()
		{
			return context;
		}

		public void setContext(Context context)
		{
			this.context = context;
		}

		public Set<String> getSelectedPHNumbers()
		{
			return selectedPHNumbers;
		}

		public void setSelectedPHNumbers(Set<String> selectedPHNumbers)
		{
			this.selectedPHNumbers = selectedPHNumbers;
		}

		public UserDetailsBean getDetailsBean()
		{
			return detailsBean;
		}

		public void setDetailsBean(UserDetailsBean detailsBean)
		{
			this.detailsBean = detailsBean;
		}

		public Location getLocation()
		{
			return location;
		}

		public void setLocation(Location location)
		{
			this.location = location;
		}
	}

}
