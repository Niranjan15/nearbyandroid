package com.niranjan.findnearby.store;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DictionaryOpenHelper extends SQLiteOpenHelper
{

	private static final int	DATABASE_VERSION		= 2;
	private static final String	DICTIONARY_TABLE_NAME	= "userFriends";
	private static final String	KEY_WORD				= "friendNumber";
	private static final String	DICTIONARY_TABLE_CREATE	= "CREATE TABLE " + DICTIONARY_TABLE_NAME + " (" + KEY_WORD + " TEXT" + ");";
	private static final String	DATABASE_NAME			= "findnearbyfriends";

	DictionaryOpenHelper(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		db.execSQL(DICTIONARY_TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		// TODO Auto-generated method stub

	}
}