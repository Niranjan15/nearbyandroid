package com.niranjan.findnearby.notification;

import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.PhoneLookup;
import android.support.v7.app.NotificationCompat;
import android.support.v7.app.NotificationCompat.Builder;
import android.util.Log;

import com.niranjan.findnearby.R;
import com.niranjan.findnearby.activity.MainActivity;
import com.niranjan.findnearby.activity.MapDisplayActivity;
import com.niranjan.findnearby.utils.AESEncryption;
import com.niranjan.findnearby.utils.ContactSearchUtil;

public class AppNotificationBuilder
{

	// private static int msgCounter = 0;

	public void notify(Context context, String contentTitle, JSONObject userDetails)
	{
		try
		{
			String phoneNumber = AESEncryption.decrypt(userDetails.getString("number"));
			StringBuilder msgBuilder = new StringBuilder();
			msgBuilder.append(ContactSearchUtil.getUserProfile(context, phoneNumber));
			msgBuilder.append(userDetails.get("msg"));
			int mId = userDetails.getInt("id");
			double latitude = userDetails.getDouble("latitude");
			double longitude = userDetails.getDouble("longitude");

			Log.d("Notificaiton", "adding notifiation for" + phoneNumber);
			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
			mBuilder.setSmallIcon(R.drawable.ic_launcher);
			mBuilder.setContentTitle(contentTitle);
			mBuilder.setContentText(msgBuilder.toString());
			mBuilder.setOnlyAlertOnce(true);
			mBuilder.setAutoCancel(true);
			boolean isNearByNotify = userDetails.getBoolean("isNearByNotify");

			Class<?> clazz = null;
			if (isNearByNotify)
			{
				clazz = MapDisplayActivity.class;
			}
			else
			{
				clazz = MainActivity.class;
			}

			Intent mapIntent = new Intent(context, clazz);
			mapIntent.putExtra("longitude", longitude);
			mapIntent.putExtra("latitude", latitude);
			mapIntent.putExtra("friendNumber", phoneNumber);
			mapIntent.putExtra("mId", mId);
			mapIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

			TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
			stackBuilder.addParentStack(clazz);
			stackBuilder.addNextIntent(mapIntent);
			PendingIntent intent = stackBuilder.getPendingIntent(mId, PendingIntent.FLAG_UPDATE_CURRENT);

			mBuilder.setContentIntent(intent);
			mBuilder.setDefaults(Notification.DEFAULT_ALL);
			mBuilder.setColor(Color.rgb(255, 255, 255));
			String userProfile = getUserProfile(context, phoneNumber);

			if (isNearByNotify)
			{
				addAdditionActions(context, mId, mBuilder, mapIntent, phoneNumber);
			}
			if (userProfile != null)
			{
				mBuilder.addPerson(userProfile);
			}

			NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			// mId allows you to update the notification later on.
			mNotificationManager.notify(mId, mBuilder.build());
			Log.d("Notificaiton", "Notifiation added for" + phoneNumber);
		}
		catch (Exception e)
		{
			Log.e("Exception", e.getMessage());
		}
	}

	private void addAdditionActions(Context context, int mId, Builder mBuilder, Intent mapIntent, String phoneNumber)
	{
		PendingIntent mapActivity = PendingIntent.getActivity(context, mId, mapIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		Intent intent = new Intent(context, MainActivity.class);
		intent.putExtra("mId", mId);
		intent.putExtra("friendNumber", phoneNumber);
		PendingIntent appActivity = PendingIntent.getActivity(context, mId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.addAction(R.drawable.gps_direction, "Map", mapActivity);
		mBuilder.addAction(R.drawable.unsync, "Remove", appActivity);
	}

	/**
	 * @param context
	 * @param phoneNumber
	 * @return
	 */
	public String getUserProfile(Context context, String phoneNumber)
	{
		Cursor cursor = null;
		try
		{
			Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
			cursor = context.getContentResolver().query(uri, new String[]
			{
					Contacts.LOOKUP_KEY,
					PhoneLookup.NUMBER,
					PhoneLookup.PHOTO_URI
			}, null, null, null);
			if (cursor == null)
			{
				return null;
			}
			try
			{
				StringBuilder uriListBuilder = new StringBuilder();
				int index = 0;
				while (cursor.moveToNext())
				{
					if (index != 0)
					{
						uriListBuilder.append(':');
					}
					uriListBuilder.append(cursor.getString(cursor.getColumnIndex(Contacts.LOOKUP_KEY)));
					index++;
					break;
				}
				return uriListBuilder.toString();
			}
			finally
			{
				cursor.close();
			}
		}
		catch (Exception e)
		{
			Log.e("Exception", e.getMessage());
		}
		finally
		{
			if (cursor != null)
			{
				cursor.close();
			}
		}

		return null;
	}
}
