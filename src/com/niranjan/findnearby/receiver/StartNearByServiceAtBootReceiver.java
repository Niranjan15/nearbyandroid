package com.niranjan.findnearby.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.niranjan.findnearby.service.LocationUpdateServiceNew;

public class StartNearByServiceAtBootReceiver extends BroadcastReceiver
{

	@Override
	public void onReceive(Context context, Intent intent)
	{
		Intent loactionUpdateService = new Intent(context, LocationUpdateServiceNew.class);
		context.startService(loactionUpdateService);
	}

}
